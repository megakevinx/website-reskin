# Client App Architecture #

![Architecture Diagram](doc/arch_diagram.png "Architecture Diagram")

This application is what’s called a __SPA__, or Single Page Application (https://en.wikipedia.org/wiki/Single-page_application). Or, more correctly, a series of SPAs. One per page The application is developed using a somewhat loose adaptation of the __MVVM architectural pattern__. Even though there are multiple JavaScript frameworks that implement MVVM-like architectures like __Angular.js__, __Knockout.js__ and __Backbone.js__, to keep things simple the choice was made to go with a manual implementation of the pattern.

There are four main architecturally significant components in the application:

## View ##

The __View__ is implemented as __Handlebars.js html templates__. This is represented in the application as a number of html template files that the __View Model__ uses (with help from Handlebars.js) to produce GUIs that the user can interact with and view a representation of the system’s state and data. Physically, this component lives as all __*.tmpl.html__ files under the __/src/templates__ directory and its sub-directories.

## View Model ##

This is the main component for the application in terms of functionality. All of the presentation and user interaction event handling logic resides in this component. The main representation of this component in the application are the __*ViewModel__ “classes” in the __/js/app/app.viewmodel.js__ and __/js/app/master.viewmodel.js__ files. __app.viewmodel.js__ serves as the entry point script file for the __edit_scholarship__ SPA. __master.viewmodel.js__ does the same for __home.html__. __AppViewModel__, being the more complex __View Model__, manages all the top level presentation of the scholarship (that is, general scholarship info as well as variants and predicates) and contains logic to respond to all the top level events. It also orchestrates the several popup dialogs that serve as data __editors__, __creators__, __viewers__ and __selectors__. All of which, conceptually, are also part of the __View Model__ component since they serve a similar purpose to __AppViewModel__, only with a more limited scope. That is: __editors__, __creators__, __viewers__ and __selectors__ are __View Models__ that only deal with their respective widgets. Physically, JavaScript files in the __/js/app/creator__, __/js/app/editor__, __/js/app/selector__, __/js/app/viewer__ and the __*.viewmodel.js__ files are what represent the __View Model__ component within the application. There’s also a number of presentation helper functions within the __/js/app/presentation__ directory which are used across the application.

## Model ##

Since most __Model__ manipulation logic is implemented on the server side and accessed through __REST API__ calls, the so-called __Model__ for the application is made up of dumb __POJO__ (Plain-old JavaScript Objects) that serve as little more than vehicles for data transmission. You can think of the __Model__ as a set of glorified __DTOs__ (Data Transfer Objects). Being a dynamic language, in a JavaScript application you don’t really have the need to have strict object definitions to simply use as __DTOs__. After all, you could simply use __JSON__ (i.e. JavaScript object literals). However, in an effort to make the code overall more readable and have a clear distinction to the purpose of certain aggrupations of data, it was decided to create a modest set of “classes” that define the objects that the __View Model__ and the __Views__ manipulate and present. I.e. The __Model__ objects. Physically these objects are defined in the JavaScript files under the __/js/app/model__ directory.

## Repository ##

Somewhat borrowing from the concepts of __Domain Driven Design___, the repository for the application is the single __AppRepository__ “class” that lives in the __/js/app/app.repository.js__ JavaScript file. It’s function in the system is to provide an interface through which the system can access the underlying data store. In our case, this is a remote server that exposes a __REST API__ that’s accessed over the Internet. So, internally, __AppRepository__ handles all the client/server communication through calls to the server side’s __REST API__.

# Client App Directory Structure #

In this section I'm going to describe the purpose of every subdirectory in the app's source code in terms of the files that they contain.

## Directory Structure ##

* 1. /src/css
* 2. /src/js
  * 2.1. /src/js/app
     * 2.1.1. /src/js/app/creator
     * 2.1.2. /src/js/app/editor
     * 2.1.3. /src/js/app/model
     * 2.1.4. /src/js/app/presentation
     * 2.1.5. /src/js/app/selector
     * 2.1.6. /src/js/app/viewer
  * 2.2. /src/js/lib
  * 2.3. /src/js/test
* 3. /src/templates
  * 3.1. /src/templates/creators
  * 3.2. /src/templates/editors
  * 3.3. /src/templates/selectors
  * 3.4. /src/templates/viewers

## 1. /src/css ##

Contains all the CSS and supporting style-related files for the app. The main CSS files are __w3.css__ which contains W3Schools standard stylization rules and the __app.css__ file which contains rules specific to the app. The rest of the files are serving the purposes of the various JavaScript libraries and JQuery plugins that are used throughout the application.

## 2. /src/js ##

Contains the bulk of the application's source code. It is organized as follows:

### 2.1. /src/js/app ###

Contains all the custom code written for the app. Main files of interest are:

- __app.js__: Contains code for app level bootstrapping and global functions definitions.
- __app.repository.js__: Contains the definition of the AppRepository class which serves as the application's main repository.
- __app.viewmodel.js__: Contains the definition of the __AppViewModel__ class which serves as the top-level __View Model__ for the __edit_scholarship.html SPA__.
- __master.viewmodel.js__: Contains the definition of the __MasterViewModel__ class which serves as the top-level __View Model__ for the __home.html SPA__.

 Within this directory, the code is organized as follows:

#### 2.1.1. /src/js/app/creator ####

Contains the definition of the __PredicateCreator__ and __PropertyCreator__ classes. These are __View Models__ that handle all presentation and event handling logic for the __Predicate Creator__ and __Property Creator__ dialog boxes.

#### 2.1.2. /src/js/app/editor ####

Contains the definitions of classes that serve as __View Models__ for all the editors for the different data types.

#### 2.1.3. /src/js/app/model ####

Contains the definitions of all the __Model__ classes.

#### 2.1.4. /src/js/app/presentation ####

Contains the definitions of the presentation helpers and utilities classes.

#### 2.1.5. /src/js/app/selector ####

Contains the definition of the __PredicateSelector__ and __PropertySelector__ classes. These are __View Models__ that handle all presentation and event handling logic for the __Predicate Selector__ and __Property Selector__ dialog boxes.

#### 2.1.6. /src/js/app/viewer ####

Contains the definitions of various viewer classes that serve as __View Models__ for dialog boxes that present read only data for different entities. 

### 2.2. /src/js/lib ###

Contains all the code for the JavaScript libraries that are used throughout the application. Among other smaller JQuery plugins, the main libraries used in the application are:

- __JQuery__ (http://jquery.com/)
- __JQuery UI__ (https://jqueryui.com/)
- __Handlebars.js__ (http://handlebarsjs.com/): used as templating engine.
- __Lodash__ (https://lodash.com/): Utilities for collection manipulation.

### 2.3. /src/js/test ###

Contains the scaffolding code for a unit test suite written using the __QUnit__ (https://qunitjs.com/) JavaScript Unit Testing framework. Because of time constraints, there aren't any test scenarios written at this time.

## 3. /src/templates ##

Contains all the __Handlebars.js__ powered html templates that are used by several GUI components throughout the application. There's one single file at the top-level of this subdirectory which is the __product.tmpl.html__. This contains the html template for the main scholarship edition table. The rest of the templates are organized as follows:

### 3.1. /src/templated/creators ###

Contains all the html template files for the __Predicate__ and __Property Creators__. For the __Predicate Creator__, there's a dedicated subdirectory __/src/templated/creators/predicate__ that contains the multiple "pieces" that make up the __Predicate__ creation dialog box. These "pieces" are called steps. The main __Predicate Creator__ template is stored in the __predicate.tmpl.html__ file while the templates for the steps are located in the __/src/templated/creators/predicate/steps__ subdirectory.

### 3.2. /src/templated/editors ###

Contains all the html template files for the different data type editors.

### 3.3. /src/templated/selectors ###

Contains all the html template files for the __Predicate__ and __Property__ selectors.

### 3.4. /src/templated/viewers ###

Contains all the html template files for the different types of viewers.

# The Editors' Hierarchy #

All of the editors for the many supported data types in the application are very similar to one another. As such, there's great opportunity for code reuse. To have some reuse and eliminate the need for duplicated code, editors use one of the core aspects of __Object Oriented Design__: __inheritance__. In terms of __UML__, the editor class hierarchy looks like the following:

## Design ##

![Editors Class Diagram](doc/editors_class_diagram.png "Editors Class Diagram")

## Implementation ##

Simple enough. Not so simple in JavaScript however. Due to JavaScript's limited support for __core OOP constructs__ like __inheritance__. A somewhat more involved strategy needs to be used to achieve the effect of __inheritance__. There are a couple of ways to do __inheritance__ in JavaScript based using prototypes. The one chosen for the implementation of the editors hierarchy is a simpler and more rudimentary one however.

In pure __Object Oriented Design__ terms, the __BaseEditor__ is an __Abstract Base Class__. Every editor that "inherits" from the __BaseEditor__ class includes the following statement among their first lines of code:

```javascript
// Setup Inheritance
var base = new BaseEditor(variantPredicateValue, editorContainerSelector, editorTemplateUrl, editorTemplateName, propertyName, getModifiedValue, saveCallback, valueContainer);
```
This is effectively instantiating a new object of the __BaseEditor__ class and assigning it to a local "__base__" variable. To use the functionality provided by their base class, editors need just to invoke the methods of the object stored in this variable.
 
 This is a common pattern among all editors except for the __ListEditor__. The __ListEditor__ is unique enough so that it didn't make sense for it to be included in the hierarchy.

### BaseEditor's responsibilities ###

__BaseEditor__ encapsulates all the basic popup presentation and template loading logic as well as event handling functionality. It also deals with notifying back to the parent (i.e. the __AppViewModel__, for the editor's case) when the data save action is to be taken.

### Concrete editors' responsibilities ###

Because much of the editor's functionality is encapsulated within the __BaseEditor__ and reused throughout the concrete editors, their responsibilities are very limited in most cases. Most concrete editors only need to be concerned about two things:

1. __They need to know certain information specific to their own html templates__ like the template's URL and identifier, among others. The BaseEditor needs to know this information so the concrete editor needs to pass those in as parameters.
2. __They also need to know the mechanism to extract the user provided input__ once a new value has been supplied for the edited field (i.e. Variant Value) and the user want's so save it. This is done by passing in a reference to a function that does this to the BaseEditor's getModifiedValue constructor parameter.

### How to implement a new editor ###

In order to implement new editors for new data types while not drifting away from the current design one would follow these steps:

__Tip__: Take a look at the implementation for __StringEditor__ which is the prototypical/simplest editor which puts all of these concepts in practice.
 
#### 1. Create a new html template for the editor ####

This is easily done by creating a new __*.tmpl.html__ file under the __/src/templates/editors directory__. The easiest way is to follow the same pattern found in the __string.tmpl.html__ template under that same directory but of course, any sort of GUI will work as well. The basic editor template structure is as follows:

```html
<div id="editor-{{DataType}}" class="data-editor" >
    <div class="title-bar">
        <a class='action-button editor-close' title="close"><i class="fa fa-window-close"></i></a>
    </div>
    <h1 class="property-name">{{propertyName}}</h1>
    <div>
        Current: <input type="text" class="editor-input" disabled="disabled" value="{{currentValue}}" />
        New: <input type="text" class="editor-input" value="{{currentValue}}" />
    </div>

    <div class="action-button-container">
        <input type="button" class="editor-submit" value="Update" />
    </div>

    <div class="bubble-triangle"></div>
</div>
```

Following this structure will produce a template for an editor that will work and be consistent with the application's overall visual design. Complete with fields for current and new values as well as a close and submit button.

#### 2. Create a new Editor concrete class ####

The next step is to create a new concrete editor class under the __/src/js/app/editor__ directory. Again, the easiest way is to use the __StringEditor.js__ file as a template. But, in a nutshell, every editor needs to know at least four pieces of information:

##### 1. editorContainerSelector #####

The JQuery selector that the __BaseEditor__ is going to use to create and present the editor. In most cases this is just the id selector of the root element of the editor's html template.

```javascript
var editorContainerSelector = "#editor-String";
```

##### 2. editorTemplateUrl #####

The relative path to the html template files that contains the markup for the editor's template.

```javascript
var editorTemplateUrl = "templates/editors/string.tmpl.html";
```

##### 3. editorTemplateName #####

A string unique to the template that's going to be used by the __Handlebars.js__ engine to identify and cache the template. This will help __Handlebars.js__ out so that it doesn't need to make a new request each subsequent time the same template is needed to be presented.

```javascript
var editorTemplateName = "stringEditor";
```

##### 4. getModifiedValue #####

A function for getting the user provided new value from the __View__. This function is different for each editor because each one of them has distinct markup and as such the way of getting to the user provided information varies.

```javascript
var getModifiedValue = function() {
    return $(editorContainerSelector).find("td.new-value input.editor-input").val();
};
```

The editor also needs to setup it's inheritance by invoking the __BaseEditor__'s constructor and storing the resulting object in a variable:

```javascript
var base = new BaseEditor(variantPredicateValue, editorContainerSelector, editorTemplateUrl, editorTemplateName, propertyName, getModifiedValue, saveCallback, valueContainer);

```

Finally, the editor's constructor function needs to return the constructed object with:

```javascript
return {
    display: base.display
};
```

Here, what's being returned is a new object which contains only a single function named display which delegates to the __BaseEditor__'s __display__ function.

Don't forget to add your newly created script file in the html page where it's going to be used. For example, in the __edit_scholarship.html__ file, add a line like the following close to the section where other scripts are being included down the bottom of the file:

```html
<script src="js/app/editor/StringEditor.js"></script>
```

#### 3. Wire up the new editor in AppViewModel ####

This is as simple as adding a new conditional statement in the __setUpProductDetailView__ function in the __AppViewModel__ class. In the __setUpProductDetailView__ function, there's a statement where JQuery is being used to wire up a click event handler to the edit floating buttons for __Variant Values__ in the main scholarship detail view:

```javascript
$(productDetailViewContainer).find('td.variant-predicate-value-container').find('a.action-button.edit').on("click", function() {
    ...
}
```
The conditional statement for invoking the new editor needs to be added in this event handler. It'd look something like this:

```javascript
if (variantPropertyDataType === PropertyDataType.String) {
    var editor = new StringEditor(variantPredicateValue, propertyName, saveVariantPredicateValueAndRefresh, cell);
    editor.display();
}
```
Here, a check is made to see what's the type of the property associated with the predicate to which the Variant Value being edited belongs. If the type is the one our new editor is designed to work with, then it is instantiated and it's display method invoked.

A number of parameters need to be passed in:
 
1. __variantPredicateValue__: The __VariantPredicateValue__ object containing the information about the Variant Value that's about to be edited.

2. __propertyName__: The name of the __Property__ associated with the __Predicate__ that the __Variant Value__ about to be edited belongs to.

3. __saveVariantPredicateValueAndRefresh__: A reference to a function defined within __AppViewModel__ that calls the __Repository__ to save the value and refreshes the __View__.

4. __cell__: the JQuery wrapped element that contains the data about to be edited within the main scholarship detail table GUI.

### Further editor examples ###

Editors for data types like string, number and boolean are more on the simpler side in terms of custom logic. Editors for date, datetime, country, state and city are somewhat more complex because their GUIs are a bit more involved. As such they serve as good examples for editors with more elaborated templates and input methods. They are indeed a bit more complex but still follow the general pattern and because of that they are pretty easy to conceptualize.

As mentioned earlier, the editor for list is by far the most complex out of the editors because it needs to deal with restricted list and restricted atomic typed properties. Still, it also follows the general editor pattern.